'use strict';

var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

var env = {
  app: 'app/',

  public: 'public',
  serve:  'public',

  scripts: 'public/js',
  css: 'public/css',
  images: 'public/images',
  fonts: 'public/fonts',

  vendors: 'public/vendors'
}

gulp.task('vendors', function() {

  gulp.src([
    'bower_components/jquery/dist/jquery.js',
    'bower_components/jquery-ui/jquery-ui.js',
    'bower_components/bootstrap/dist/js/bootstrap.js',
  ])
    .pipe($.concat('vendors.min.js'))
    .pipe($.uglify())
    .pipe(gulp.dest(env.vendors))

  gulp.src([
    'bower_components/jquery-ui/themes/ui-darkness/jquery-ui.css',
    'bower_components/jquery-ui/themes/ui-darkness/theme.css',
    'bower_components/bootstrap/dist/css/bootstrap.css'
  ])
    .pipe($.concat('vendors.css'))
    .pipe(gulp.dest(env.vendors))

  gulp.src([
    'bower_components/jquery-ui/themes/ui-darkness/images/**/*'
  ])
    .pipe($.imageOptimization({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    }))
    .pipe(gulp.dest(env.vendors + '/images'))

  gulp.src([
    'bower_components/html5shiv/dist/html5shiv.js',
    'bower_components/respond/dest/respond.min.js'
  ])
    .pipe(gulp.dest(env.vendors))

  gulp.src([
    'bower_components/bootstrap/dist/fonts/**/*'
  ])
    .pipe(gulp.dest(env.vendors + '/fonts'))
});

gulp.task('coffee', function() {
  gulp.src(env.app + 'coffee/**/*.coffee')
    // .pipe($.order([], {base: '.'}))
    .pipe($.concat('application.min.js'))
    .pipe($.coffee())
    .pipe($.uglify())
    .pipe(gulp.dest(env.scripts))

});

gulp.task('styles', function() {

  gulp.src(env.app + 'less/**/*.less')
    // .pipe($.order([], {base: '.'}))
    // .pipe($.concat('application.css'))
    .pipe($.less())
    .pipe($.autoprefixer({
        // browsers: ['last 2 versions'],
        cascade: false
    }))
    // .pipe($.cssmin())
    .pipe(gulp.dest(env.css))
});

gulp.task('images', function() {
  gulp.src([
    env.app + 'images/**/*.jpg',
    env.app + 'images/**/*.png',
    env.app + 'images/**/*.gif',
    env.app + 'images/**/*.jpeg'
  ])
    .pipe($.imageOptimization({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    }))
    .pipe(gulp.dest(env.images))
});

gulp.task('fonts', function() {
  gulp.src([
    env.app + 'fonts/**/*.otf',
    env.app + 'fonts/**/*.ttf'
  ])
    .pipe($.fontgen({
      dest: env.fonts
    }))
});

gulp.task('templates', function() {
  gulp.src([
    env.app + 'templates/**/*.jade',
    '!' + env.app + 'templates/mixins/**/*.jade'
  ])
    .pipe($.jade({
      pretty: true
    }))
    .pipe(gulp.dest(env.public))
});

gulp.task('watch', function() {
  gulp.watch([env.app + 'coffee/**/*.coffee'],  ['coffee'])
  gulp.watch([env.app + 'less/**/*.less'],      ['styles'])
  gulp.watch([env.app + 'templates/**/*.jade'], ['templates'])
});

gulp.task('serve', $.serve(env.serve));

gulp.task('build',   ['vendors', 'coffee', 'styles', 'images', 'fonts', 'templates']);
gulp.task('default', ['watch', 'serve'])