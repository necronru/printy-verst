# Printy

Верстка макетов для стартапа "Printy"

# Get started
```bash
npm install
bower install
gulp
```

## Gulp tasks

```bash
	
# запускает локальный сервер на порту :3000
gulp serve
	
# зырит в файлы и собирает проект "на лету"
gulp watch

# собирает проект
gulp build

# конверит и собирает css для шрифтов
gulp fonts

# сжимает и оптимизирует изображения
gulp images

# компилит LESS и конкатанирует CSS в один файл
gulp styles

# компилит CoffeeScript
gulp coffee

# компилит всех вендоров из bower_components в vendors.min.js
gulp vendors

# собирает jade шаблоны
gulp templates
```